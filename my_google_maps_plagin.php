<?php
/*
Plugin Name: google_map_js_api_plagin
Plugin URI: http://wordpress.loc
Description: shows your shops on the map
Version: 1.0
Author: NoComments
Author URI: http://wordpress.loc
License: GPLv2
*/

add_action( 'wp_enqueue_scripts', 'my_script' );

function my_script() {
	global $post;
	$script_url = plugins_url( 'js/my_google_maps_plagin.js', __FILE__ );
	wp_enqueue_script( 'google_map_script', $script_url, array( 'google_map_init' ) );
	wp_localize_script( 'google_map_script', 'soc', [
		'name' => get_post_meta( $post->ID, 'name', true ),
		'lat'  => get_post_meta( $post->ID, 'lat', true ),
		'lng'  => get_post_meta( $post->ID, 'lng', true ),
		'desc' => get_post_meta( $post->ID, 'desc', true ),
		'loc'  => get_post_meta( $post->ID, 'loc', true ),
	] );
}

add_action( 'wp_enqueue_scripts', 'google_maps_init' );

function google_maps_init() {
	$map_src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyCfIEaNUAxFxENNP_R8NO_2pUlJXbKrsxQ";
	wp_enqueue_script( 'google_map_init', $map_src, [ 'jquery' ] );
}

add_action( 'wp_print_styles', 'google_maps' );

function google_maps() {
	$script_url = plugins_url( 'css/style_google.css', __FILE__ );
	wp_enqueue_style( 'google_maps', $script_url );
}

add_action( 'init', 'register_post_shop' );

function register_post_shop() {
	register_post_type( 'shops', array(
		'labels'          => array(
			'name'          => 'Shops',
			'singular_name' => 'Shop',
			'add_new'       => 'Add Shops',
			'add_new_item'  => 'Add Shop',
			'edit_item'     => 'Edit Shop ',
			'view_item'     => 'view Shop',
			'new_item'      => 'new Shop',
			'search_items'  => 'search Shop',
			'not_found'     => 'Shops not found',
		),
		'public'          => true,
		'menu_position'   => 14,
		'supports'        => array( 'title', 'editor', 'comments' ),
		'capability_type' => 'post',
		'taxonomies'      => array( 'status' ),
		'has_archive'     => true,
	) );
}

add_action( 'add_meta_boxes', 'add_shop' );

function add_shop() {
	add_meta_box( 'shop', 'Discription', 'add_shop_metabox_info', 'shops', 'side' );
}

function add_shop_metabox_info() {
	global $post;

	echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
	     wp_create_nonce( plugin_basename( __FILE__ ) ) . '" />';

	$name = get_post_meta( $post->ID, 'name', true );
	$desc = get_post_meta( $post->ID, 'desc', true );
	$loc  = get_post_meta( $post->ID, 'loc', true );

	echo '<p>Shop name:</p>';
	echo '<input type="text" name="name" value="' . $name . '"  />';
	echo '<p>Shop description:</p>';
	echo '<input type="text" name="desc" value="' . $desc . '"   />';
	echo '<p>Shop location:</p>';
	echo '<input type="text" name="loc" value="' . $loc . '"   />';

}

add_action( 'save_post', 'save_shop_meta', 1, 2 );

function save_shop_meta( $post_id, $post ) {

	if ( ! wp_verify_nonce( $_POST['eventmeta_noncename'], plugin_basename( __FILE__ ) ) ) {
		return $post->ID;
	}

	if ( ! current_user_can( 'edit_post', $post->ID ) ) {
		return $post->ID;
	}

	$shops_meta['name'] = strip_tags( trim( $_POST['name'] ) );
	$shops_meta['desc'] = strip_tags( trim( $_POST['desc'] ) );
	$shops_meta['loc']  = strip_tags( trim( $_POST['loc'] ) );
	$address            = str_replace( ' ', '+', $shops_meta['loc'] );
	$map_src            = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . $address . '&key=AIzaSyCfIEaNUAxFxENNP_R8NO_2pUlJXbKrsxQ';
	$content            = file_get_contents( $map_src );
    $content            = json_decode( $content );
    $lat                = $content->results[0]->geometry->location->lat;
	$lng                = $content->results[0]->geometry->location->lng;
	$shops_meta['lat']  = $lat;
	$shops_meta['lng']  = $lng;

	foreach ( $shops_meta as $key => $value ) {
		if ( $post->post_type == 'revision' ) {
			return;
		}
		$value = implode( ',', (array) $value );
		if ( get_post_meta( $post->ID, $key, false ) ) {
			update_post_meta( $post->ID, $key, $value );
		} else {
			add_post_meta( $post->ID, $key, $value );
		}
		if ( ! $value ) {
			delete_post_meta( $post->ID, $key );
		}
	}
}

add_shortcode( 'short_google_map', 'add_google_map' );

function add_google_map() {

	$args   = array( 'post_type' => 'shops' );
	$params = [];

	$mypost = get_posts( $args );
	foreach ( $mypost as $pos ) {
		$params [] = [
			'name' => get_post_meta( $pos->ID, 'name', true ),
			'loc'  => get_post_meta( $pos->ID, 'loc', true ),
			'lng'  => get_post_meta( $pos->ID, 'lng', true ),
			'lat'  => get_post_meta( $pos->ID, 'lat', true ),
		];
	}

	if ( ! empty( $params ) ) {

		$shotcode_url = plugins_url( 'js/shortcode_map.js', __FILE__ );
		wp_enqueue_script( 'shortcode_map', $shotcode_url, [ 'google_map_init' ] );
		wp_localize_script( 'shortcode_map', 'loc', $params );

		return '<div id="main_map">';

		return '</div>';
	}


}


